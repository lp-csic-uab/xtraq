#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
xtraq_36.py
(xtraq_37)
"""
#
import threading
import cPickle
import os
import wx
import traceback
import zipfile as zipf
from time import time, sleep
from cStringIO import StringIO
from numpy import matrix
from scipy.linalg import solve
from xtraq_text import *
from xtraq_anzreader import extract_anz, xmltags
from xtraq_warnings import error_fatal, aviso
from xtraq_datafiller import get_full_data, set_global_itraq
from xtraq_datafiller import get_spect_from_txt, get_itraq_ions
from xtraq_datafiller import add_max_ions
#
#
testfile = r'C:\XANZ\50test.anz'
DIRTEMP = r'C:\temp'
FILTRAR = 0
FILEFORBLAST = r'C:\forblast.txt'
#
total_saved = total_fa = 0
report_thread = []
sem = threading._Semaphore()
#
#
def run(frame):
    """"""
    source = frame.source
    es_anz = frame.property['tp']
    _filter = False
    if frame.property['op'] == FILTRAR:
        _filter = True
    else:
        frame.bt_siit.SetBackgroundColour("red")
    #
    if frame.property['bcht']:
        sourcelist = [source + os.sep + item for item in os.listdir(source)]
    else:
        sourcelist = [source]
    #
    set_global_itraq(frame.property['calidata'])
    #
    if es_anz:
        sourcelist = check_if_zip(sourcelist)
        if len(sourcelist) == 0:
            aviso("no hay file para procesar")
            remove_mani_clock(frame.maniclock)
            return
        run_anz(frame, sourcelist, _filter)
    else:
        run_dta(frame, sourcelist, _filter)
    #
    frame.bt_run.Enable(True)
    #
    if os.path.exists(frame.excel):
        frame.bt_siit.SetBackgroundColour("green")
    frame.SetStatusText('LISTO')
    #
    remove_mani_clock(frame.maniclock)
    return
#
#
def remove_mani_clock(maniobject):
    """"""
    try:
        maniobject.Destroy()
    except wx._core.PyDeadObjectError:
        pass
#
#
def check_if_zip(file_list):
    """"""
    new_files = []
    for item in file_list:
        if zipf.is_zipfile(item):
            new_files.append(item)
        else:
            aviso(NOSEINCLUIRA % item)
    return new_files
#
#
def run_dta(frame, sources, _filter):
    """
    full_data = list of dictionaries generated from a collection of dtas.
    Each dictionary stores data from a dta file
    Each dictionary key points to a list of one or more items
    '_filter' is there by analogy with run_anz parameters
    """
    excel = frame.excel
    if not excel.strip():
        aviso('Excel file does not exist')
        return
    #
    properties = frame.property    # dictionary con el estado/valor en los widgets
    #
    frame.bt_run.Enable(False)
    vers = os.sys.winver
    txt = 'running %s in Python vs %s' % (frame.source, vers)
    frame.SetStatusText(txt)
    #
    full_data = []
    time0 = time()
    #
    for dir_dtas in sources:
        dictios = extract_dtas(dir_dtas, properties['cffitraq'])
        full_data.extend(dictios)
    #
    report = produce_report(excel, full_data, properties, time0)
    notify_outcome(frame, report)
#
#
def run_anz(frame, sources, _filter):
    """
    fulldata = lista de diccionarios generada a partir de la coleccion de anns
    Cada diccionario almacena la informacion de un archivo ann
    Cada key del diccionario se�ala a una lista que contiene uno o mas items
    """    
    target = frame.target
    excel = frame.excel
    propiedades = frame.property
    #
    frame.bt_run.Enable(False)
    vers = os.sys.winver
    txt = 'running %s in Python vs %s' % (sources, vers)
    frame.SetStatusText(txt)
    #
    if _filter:
        if not target.strip():
            aviso('No existe file target')
            return
        report = filter_anz_itraq(sources, target,
                                  frame, propiedades['cffitraq'])
    else:
        if not excel.strip():
            aviso('No excel file exists')
            return
        time0 = time()
        full_data = preparar_data(sources, propiedades)
        compacta(full_data, tipo="anz")
        report = produce_report(excel, full_data, propiedades, time0)
        if not report:
            aviso('No report generated')
            return
    #
        if propiedades['xprt']:
            excelpart = excel.rsplit('.') 
            file4blast = excelpart[0] + '_4blast.txt'
            xport_for_blast(full_data, file4blast)
            report += '\nArchive for BLAST in ', file4blast
    #    
    notify_outcome(frame, report)
#
#
def xport_for_blast(full_data, arch):
    """
    Stores selected PEAKS tags in a text file.
    One tag per line to be directly read by XBlast.
    Deberia a�adirse la informacion del scan (hay que revisar compatibilidad
    with XBlast format --> just put a line with '> data'
    """
    peptide_tags = []
    for dictionary in full_data:
        new_tags = dictionary['tag'][0].split(' | ')
        info = '> %s %s' % (dictionary['SCAN'][0], dictionary['scx'][0])
        for item in new_tags:
            if item:
                peptide_tags.append(info)
                peptide_tags.append(item)
     
    txt = '\n'.join(peptide_tags)
    open(arch, 'w').write(txt)
#
#
def produce_report(excel, full_data, propiedades, time0=0):        
    """Already common to anz and dta.
    """
    report = []
    time1 = time()
    crea_referencias(full_data, propiedades)
    full_txt = generate_xls(full_data, propiedades)
    if not full_txt:
        return None
    time2 = time()
    save_xls(full_txt, excel)
    time3 = time()
    #
    report.append('time prepare_data    %.2f' % time1 - time0)
    report.append('time generate_xls    %.2f' % time2 - time1)
    report.append('time save_xls        %.2f' % time3 - time2)
    report.append('             total   %.2f' % time3 - time0)
    #
    return report
#
#
def notify_outcome(frame, report):
    """"""
    outcome = '\n'.join(report)
    frame.SetStatusText('done')
    frame.tc_sec.SetValue(outcome)
#
#
def filter_anz_itraq(source, target, frame, cutoff):
    """"""
    global total_saved, total_fa, report_thread
    report_thread = []
    total_saved = 0
    total_filtered = 0
    total_gni = 0
    total_fa = 0
    total_sa = 0
    report = []
    report_f = []
    report_gnil = None
    #
    try:
        cutoff = int(float(cutoff))
    except ValueError:
        cutoff = 0    
    #
    t0 = time()
    filecount = 0
    more_than_one = len(source) > 1
    for arch_anz in source:
        t1 = time()
        no_itraq_list, report_gnil = get_no_itraq_list(arch_anz, target, cutoff)
        num_borrar = len(no_itraq_list)
        t2 = time()
        tarea = threading.Thread(target=for_thread,
                                 args=(arch_anz, no_itraq_list, target))
        tarea.start()
        #
        t3 = time()
        #
        filecount += 1
        #
        par_gni = t2 - t1
        par_sa = t3 - t2
        par_tot = t3 - t1
        #
        report.append('#%i -> %s \n' % (filecount, arch_anz))
        report.append('time get_no_itraq  %.2f' % par_gni)
        report.extend(report_gnil)
        report.append('time threading     %.2f' % par_sa)
        report.append('               total %.2f' % par_tot)
        #
        report.append('\n*** filtered %i ***"' % num_borrar)
        #
        if more_than_one:
            frame.tc_sec.SetValue("#%i --> file %s  processed in %.3f seconds"
                                  % (filecount, arch_anz, par_tot))
            #
            total_filtered += num_borrar
            total_gni += par_gni
            total_sa += par_sa
        else:
            while True:
                t4 = time()
                sleep(3)
                frame.tc_sec.SetValue("Saving file: %s" % arch_anz)
                #print threading.activeCount()
                if threading.activeCount() == 2:
                    report.extend(report_thread)
                    report.append('*** time_total %.2f ***\n' % t4 - t1)
                    return report
    #
    while True:
        t4 = time()
        sleep(4)
        frame.tc_sec.SetValue("Saving files: %i left"
                              % (threading.activeCount() - 2))
        if threading.activeCount() == 2:
            report_f.append('%i file treated\n' % filecount)
            report_f.append('time get_no_itraq  %.2f' % total_gni)
            report_f.extend(report_gnil)
            report_f.append('time filter_anz      %.2f' % total_fa)
            report_f.append('               total %.2f' % t4 - t0)
            #
            report_f.append('\n*** saved %i, filtered %i ***' % (total_saved, total_filtered))
            report_f.append('***   time_total %.1f   ***' % t4 - t0)
            report_f.extend(report_thread)
            #
            return report_f
#
#
def for_thread(arch_anz, no_itraq_list, target):
    """"""
    global total_saved, total_fa, report_thread
    #
    t0 = time()
    num = filter_anz(arch_anz, no_itraq_list, target)
    t1 = time()
    #
    sem.acquire()
    total_saved += num
    total_fa += t1 - t0
    report_thread.append("saved %i from %s" % (num, arch_anz))
    sem.release()
#
#
def get_no_itraq_list(arch=testfile, target='', cutoff=5):
    """
    Only for anz's and is an option selected with "filtrar".
    For dtas it is directly made in extract_dtas().
    Maybe should be homogenized...
    REPORT AND FILTER SHOULD NOT BE EXCLUSIVE FOR ANZ
    """
    tags = ['peaks']
    listaborrado = []
    informe = []
    time0 = time()
    dictionaries = extract_anz(arch, tags)
    time1 = time()
    informe.append('.... time extract_anz  %.2f' % (time1 - time0))
    time_get_spec = 0
    time_get_itraq = 0
    #
    for diccio in dictionaries:
        try:
            spectrum = (diccio['peaks'])[0]
        except (IndexError, TypeError):
            spectrum = None
        if not spectrum:
            break
        time0 = time()
        spec = get_spect_from_txt(spectrum)
        time1 = time()
        itraq_ions = get_itraq_ions(spec)
        time2 = time()
        #
        time_get_spec += time1 - time0
        time_get_itraq += time2 - time1
        #
        if max(itraq_ions) < cutoff:
            listaborrado.append(diccio['name'][0])
    #
    informe.append('.... time get_spec   %.2f' % time_get_spec)
    informe.append('.... time get_itraq  %.2f' % time_get_itraq)
    #
    return listaborrado, informe
#
#
def filter_anz(arch_anz, listaborrado, target):
    """"""
    count = filter_anns(arch_anz, listaborrado, target)
    #filtra_index(arch, listaborrado)
    return count
#
#
def filter_anns(arch_anz, delete_list, target):
    """this is the slower function
    if target is a folder then works in batch
    There is an error in zipfile due to unicode:
        Traceback (most recent call last):
        ...
        File "D:\Python23\lib\zipfile.py", line 166, in FileHeader
            return header + self.filename + self.extra
        UnicodeDecodeError: 'ascii' codec can't decode byte 0xd5 in
        position 10: ordinal not in range(128)

    The code could be fixed in zipfile.py:
    return header + self.filename + self.extra   -->
    return header + self.filename.encode("utf-8") + self.extra
    """
       
    if os.path.isdir(target):
        dir_original, filename = os.path.split(arch_anz)
        anz_file = target + os.sep + filename
    else:
        anz_file = target
    
    oldfilezip = zipf.ZipFile(arch_anz, 'r')
    nwfilezip = zipf.ZipFile(anz_file, 'w', compression=zipf.ZIP_DEFLATED)
    #
    nwfilezip.writestr('index.ann', oldfilezip.read('index.ann'))
    nwfilezip.close()
    #
    nwfilezip = zipf.ZipFile(anz_file, 'a', compression=zipf.ZIP_DEFLATED)
    count = 0
    #
    listfile = oldfilezip.namelist()
    listfile.remove('index.ann')
    #
    for ann in listfile:
        if ann not in delete_list:
            nwfilezip.writestr(ann, oldfilezip.read(ann))
            count += 1
    
    return count
# 
#
def clean_dirs(directories):
    """"""
    for _dir in directories:
        for archive in os.listdir(_dir):
            archive = _dir + os.sep + archive
            if os.path.isfile(archive):
                os.remove(archive)
#
#
def delete_from_index(arch, to_delete):
    """no implemented"""
    pass
#
#
def preparar_data(source, propiedades):   
    """Extracts the anz archive and fills the dictionary in several steps (!!)
    -->dictionaries = extract_anz(arch, xmltags)
    -->add_max_ions(diccio, spec, frg_number)
    -->add_itraq_ions(diccio, spec)
    -->make_tags_from_pconf(diccio, cutoff_tags)
    -->add_best_sequence(diccio, cutoff_sequence)
    -->add_data(diccio, directory, scx)
    """
    full_data = []
    cutoff_secuence = float(propiedades['cffscr'])
    cutoff_tags = float(propiedades['cfftgs'])
    frg_number = propiedades['specdata']['total']
    #print "max_fragments= ", frg_number
    
    for arch in source:
        _dir, scx = os.path.split(arch)
        dicciolist = extract_anz(arch, xmltags)

        arch_data = get_full_data(dicciolist, _dir, scx, frg_number,
                                  cutoff_tags, cutoff_secuence)

        full_data.extend(arch_data)
    return full_data
#
#
def quant_itraq(A, intens):
    """"""
    b = matrix(intens).transpose()
    q_matrix = solve(A, b).transpose()
    return ['%.1f' % item for item in q_matrix[0]]
#
#
def generate_xls(full_data, properties=None):
    """"""
    full_txt = []
    names = ['scx', 'SCAN', 'mass_p', 'charge', 'ref', 'bseq', 'bscore', 'tag', 'fpks', 'itrq', 'quant']
    checklist = ['scx', 'scn', 'mp', 'chg', 'ref', 'bseq', 'bscr', 'pktgs', 'fpks', 'itrq', 'quant']
    headers = []
    #
    try:
        diccio_muestra = full_data[0]
    except IndexError:
        return ""
    #
    listado = [name for name, key in zip(names, checklist) if properties[key]]
       
    # add quantification
    if 'quant' in listado:
        A = matrix(properties['isodata'])
        diccio_muestra['quant'] = ['a', 'b', 'c', 'd']
        for dictionary in full_data:
            dictionary['quant'] = quant_itraq(A, dictionary['itrq'])
    #
    # prepare header
    for tag in listado:
        nitems = len(diccio_muestra[tag])
        if nitems > 1:
            for n in range(1, nitems + 1):
                tag_long = tag + "_" + str(n)
                headers.append(tag_long)
        else:
            headers.append(tag)
    
    header = '\t'.join(headers)
    #
    # prepare data
    for dictionary in full_data:
        txtdata = []
        for tag in listado:
            txtdata.extend([item for item in dictionary[tag]])

        txt = '\t'.join(txtdata)
        full_txt.append(txt)
    #
    xlsdoc = '\n'.join(full_txt)
    xlsdoc = header + '\n' + xlsdoc
    return xlsdoc
#
#
def save_xls(text, file_path):
    """"""
    try:
        open(file_path, 'w').write(text)
    except IOError:
        aviso(DESPISTADO)
        file_path = r'C:\despistado.xls'
        open(file_path, 'w').write(text)
#
#
def get_matching(mass_p, fragments, mass_memory, par_win, frg_win, min_macht):
    """Search identical spectra using the mass_memory dictionary.
    ---> mass_memory[rounded_mass] = [mp, [fragments], ref]
    To speed up the search, la key del diccionario es la masa entera
    In this way es facil seleccionar todos los espectros con una masa
    entre una unidad inferior o superior a la central y comparar solo
    con estos y no con toda la coleccion. 
    """
    mass = int(mass_p)
    # 
    for key_mass in [mass - 1, mass, mass + 1]:
        if key_mass not in mass_memory:
            continue
        for item in mass_memory[key_mass]:
            mass = item[0]
            if mass - par_win < mass_p < mass + par_win:
                equals = 0
                for mz in item[1]:
                    for mzold in fragments:
                        if (mzold - frg_win) < mz < (mzold + frg_win):
                            equals += 1
                        if equals > min_macht - 1:
                            return key_mass, item[2]
    return None, None
#
#
def get_matching_vector(mass_p, fragments, mass_memory, par_win, frg_win, min_match,
                        critical_score=None, spectrum=None):
    """Not implemented. Like "get_coincidente" but comparison would be made with vectors (doc_product).
    Para ello habr�a que tener acceso a los espectros completos ya almacenados.
    Aqui "fragments" y item[1] serian los n primeros fragmentos de los espectros
    con 20>n>5. El numero de fragmentos almacenados en 'fpks' puede ajustarse al
    valor que se quiera desde get_full_data(...,frg_number,...). frg_number se
    asigna en la GUI de class_spc.
    """
    mass = int(mass_p)
    #
    for key_mass in [mass - 1, mass, mass + 1]:
        if key_mass not in mass_memory:
            continue
        for item in mass_memory[key_mass]:
            mass = item[0]
            if mass - par_win < mass_p < mass + par_win:
                score = dot_product(fragments, spectrum[item])
                if score < critical_score:
                    return key_mass, item[2]
    return None, None
#
#
def dot_product(fragment, item):
    """To be implemented"""
    return 1
#
#
def crea_referencias(full_data, propiedades):
    """ Esta funcion ahora asigna una referencia unica a cada espectro diferente
    Si se desea puede almacenar en formato Pickle un diccionario XXX.idx: 
    mass_memory[masa_redondeada]= [mp, [fragments], ref]
    Este diccionario se puede utilizar para leerlo y utilizarlo con otras
    files en un estudio a largo plazo de forma que las referencias seran
    consistentes. Para ello debe chequearse la opcion correspondiente.
    Esta funcion debe derivar a dos comparaciones alternativas distintas:
    La normal que determina si al menos n (min_macht) de m fragmentos coinciden
    y la vectorial que todavia no esta implementada.
    Para ello diccio['fpks'] y [fragments] deberian contener los ca. 20 fragmentos
    mas altos (y sus intensidades) para la comparacion vectorial.
    """
    use_idx = propiedades['idx']
    spec_param = propiedades['specdata']
    par_win = spec_param['par'] / 1000.
    frg_win = spec_param['frg'] / 1000.
    min_macht = spec_param['min']
    metodo = spec_param['mthd']
    #
    ref = 0
    mass_memory = {}
    index = propiedades['fidx']
    #
    if use_idx:
        try:
            mass_memory, ref = read_index_references(index)
        except (IOError, UnboundLocalError):
            print "589 no puedo leer indice ", index        
    #
    if metodo == 0:
        for diccio in full_data:
            #
            try:
                mass_p = float(diccio['mass_p'][0])
            except IndexError:
                return None
            #
            fragments = [float(ion) for ion in diccio['fpks']]
            #
            (keymass, the_ref) = get_matching(mass_p, fragments, mass_memory,
                                              par_win, frg_win, min_macht)
            #
            if keymass is None:
                ref += 1
                the_ref = ref
                keymass = int(mass_p)
                if keymass in mass_memory:
                    mass_memory[keymass].append([mass_p, fragments, the_ref])
                else:
                    mass_memory[keymass] = [[mass_p, fragments, the_ref]]
                 
            diccio['ref'] = [str(the_ref)]
    #
    elif metodo == 1:
        aviso("metodo vectorial no implementado\nno se crearan referencias")
        for diccio in full_data:
##            #
##            try:
##                mass_p = float(diccio['mass_p'][0])
##            except IndexError:
##                return None
##            #
##            fragments = [float(ion) for ion in diccio['peaks'].sort[0:20]]
##            #
##            (keymass, the_ref) = get_coincidente_vector(mass_p, fragments,
##                                                        mass_memory, par_win,
##                                                        frg_win, min_macht)
##            #
##            if keymass is None:
##                ref += 1
##                the_ref = ref
##                keymass = int(mass_p)
##                if keymass in mass_memory:
##                    mass_memory[keymass].append([mass_p, fragments, the_ref])
##                else:
##                    mass_memory[keymass] = [[mass_p, fragments, the_ref]]
##                 
##            diccio['ref'] =[str(the_ref)]    
            
            diccio['ref'] = ['0']
            
    if use_idx:
        try:
            store_index_references(index, mass_memory, ref)
        except UnboundLocalError:
            pass

def read_index_references(indice):
    """"""
    return cPickle.load(open(indice))

def store_index_references(indice, diccio, ref):
    """"""
    guardar = [diccio, ref]
    cPickle.dump(guardar, open(indice, 'w'))
#
# 
def get_mix_diccio(dnew, dold):
    """
    Not sure is working correctly
    Previously the 2 best sequences per spectra have been selected
    but due to the min score maybe one is eliminated.
    Wnen se juntan dos spectra es possible que entre otra mucho peor
    que la ya eliminated  del segundo espectro
    """
    #
    sc1 = float(dnew['bscore'][0]), dnew['bseq'][0], dnew['carga'][0]
    sc2 = float(dnew['bscore'][1]), dnew['bseq'][1], dnew['carga'][0]
    sc3 = float(dold['bscore'][0]), dold['bseq'][0], dold['carga'][0]
    sc4 = float(dold['bscore'][1]), dold['bseq'][1], dold['carga'][0]
    #
    full = [sc1, sc2, sc3, sc4]
    #print full
    full.sort(reverse=True)
    #
    dold['carga'] = [full[0][2] + "," + full[1][2]]
    dold['bscore'] = [str(full[0][0]), str(full[1][0])]
    dold['bseq'] = [full[0][1], full[1][1]]
    newtags = dnew['tag'][0] + ' | ' + dold['tag'][0]
    newtags = newtags.strip(' | ')
    dold['tag'] = [newtags]
    
    return dold
#
# 
def compacta(full_data, tipo, score=0.4):
    """removes repeated scans in the same file:
            --> same scan, different charge, different tags.
        Pone la informacion en uno solo de los scans
        el parametro "tipo" existe para poder distinguir en el futuro la
        compactacion de anz (con tags) y de dtas (sin tags)
        the "score" paramenteres para corregir el problema de add_best_sequence()
    """
    end = len(full_data)
    scan_memory = {}
    save_for_compact = []
    #
    for idx in range(0, end):
        #
        diccio = full_data[idx]
        scan = diccio['SCAN'][0]
        fname = diccio['scx'][0]
        #
        found = False
        if scan in scan_memory:
            for name, index in scan_memory[scan]:
                if fname == name:
                    #print "yes******yes is same scan of same file"
                    diccio_old = full_data[index]
                    mixed_dict = get_mix_diccio(diccio, diccio_old)
                    full_data[idx] = mixed_dict           # redundant!!!
                    save_for_compact.append(idx)
                    found = True
        if not found:
            try:
                scan_memory[scan].append((fname, idx))
            except KeyError:
                #print "error"
                scan_memory[scan] = [(fname, idx)]
    
    save_for_compact.sort(reverse=True)
    for idx in save_for_compact:
        #print "compacting", idx
        full_data.pop(idx)
#
#
def extract_dtas(dir_dtas, cutoff=0):
    """
    dtas haveno tags like anz, thus hay que buscar los datos por
    su posicion en el texto, cosa que puede variar con el time...
    Extract_dtas de paso filtra spectros sin iones itraq.
    """
    cutoff = int(cutoff)
    dicciolist = []
    scan_old = ''
    dummy, scx = os.path.split(dir_dtas)
    #
    for eachfile in os.listdir(dir_dtas):
        try:
            diccio = {}
            file_atoms = eachfile.split('.')
            scandata = file_atoms[-4]
            #
            if scandata == scan_old:
                continue
            diccio['scx'] = [scx]
            #diccio['name'] = [eachfile]
            diccio['SCAN'] = [scandata]
            scan_old = scandata
            #
            fullpath = dir_dtas + r'/' + eachfile
            full_dta = [txtline.split() for txtline in open(fullpath, 'r')]
            head = full_dta[0]
            spec = full_dta[1:]
            #
            spec = [(int(intensity), float(mass)) for mass, intensity in spec]
            #
            itraq = get_itraq_ions(spec)
            #
            if max(itraq) < cutoff:
                continue
            #
            diccio['itrq'] = ['%i' % itraq[0], '%i' % itraq[1],
                              '%i' % itraq[2], '%i' % itraq[3]]
            add_max_ions(diccio, spec)
            #
            carga = int(head[1])
            diccio['carga'] = [str(carga)]
            diccio['mass_p'] = ['%.3f' % ((float(head[0]) + carga - 1) / carga)]
            #
            dicciolist.append(diccio)
        except error_fatal:
            raise
        except Exception:
            f = StringIO()
            traceback.print_exc(file=f)
            valor = f.getvalue()
            aviso(valor)            
            go_on = aviso(NOSEINCLUIRA % eachfile, True)
            if not go_on:
                raise
    #
    return dicciolist
     
            
if __name__ == "__main__":
       
    anzarch = r'C:\XTRAQ\50bwm1.anz'
    ftarget = r'C:\XTRAQ\50bwm1r.anz'
    print "done", filter_anz(anzarch, [], ftarget)
    
    #pass
    #make_itraq_only_anz(testfile, 5)
    #test_tagslecture(testfile, xmltags)
     
## ##for shell
## import zipfile as zipf
## testfile = r'C:\50mM.anz'
## filezip = zipf.ZipFile(testfile, 'r')
## file1=filezip.filelist[1]
## file1.filename                      ## --> 'spectra/507.41666_2.ann'