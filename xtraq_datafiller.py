#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
xtraq_datafiller_33.py
(xtraq_36)
"""
#
import os
from xtraq_warnings import aviso
#
#
def set_global_itraq(calidata):
    """
    to give global values to valmxxx/valMxxx ?
    for get_itraq to work with the values I want...
    In this version the value 114.2 is given as exact for the itraq reagent of lower mass
    It still must be calculated exactly to correct this.
    """
    
    global valm114, valm115, valm116, valm117 
    global valM114, valM115, valM116, valM117 
    
    val114 = 114.2 + calidata[0] / 1000.
    desv = calidata[1] / 1000.
      
    valm114 = val114 - desv
    valm115 = valm114 + 1
    valm116 = valm114 + 2
    valm117 = valm114 + 3
    ##
    valM114 = val114 + desv
    valM115 = valM114 + 1
    valM116 = valM114 + 2
    valM117 = valM114 + 3
#
#
def get_full_data(dicciolist, directory, scx, frg_number, cutoff_tags, cutoff_secuence):
    """"""
    full_data = []
    for diccio in dicciolist:
        spect = diccio['peaks']
        #
        try:
            spec = get_spect_from_txt(spect[0])
            add_max_ions(diccio, spec, frg_number)
            add_itraq_ions(diccio, spec)
        except IndexError:
            diccio['itrq'] = ['0', '0', '0', '0']
            diccio['fpks'] = ['0'] * frg_number
        #
        make_tags_from_pconf(diccio, cutoff_tags)
        add_best_sequence(diccio, cutoff_secuence)
        add_data(diccio, directory, scx)
    full_data.extend(dicciolist)
    return full_data
#
#
def get_spect_from_txt(spectro):
    """
    Return spectrum in format [[I1,M1],[I2,M2], ...,[In,Mn]]
    with intensities in first position and converted to float
    """
    #print spectro
    spec = [txtline.split() for txtline in spectro.splitlines()]
    #print spec
    revpectro = [(float(intensity), float(mass)) for mass, intensity in spec]
    
    return revpectro
#
#
def add_max_ions(diccio, spect, n=4):
    """n esta por si se quieren anadir mas iones
       modificado en vs3.1 (mas rapido):
       lista=['%.2f' %item for item, dummy in spec[0:n]]
       diccio['fpks'].extend(lista)
    """
    spec = spect[:]
    spec.sort(reverse=True)
    #
    diccio['fpks'] = []
    new = spec[0:n]
    try:
        lista = ['%.2f' % item for dummy, item in new]
    except TypeError:
        aviso("the groups of ions gave an error in add_max_ions")
        raise
    diccio['fpks'].extend(lista)
#
#
def add_itraq_ions(diccio, spec):
    """"""
    a, b, c, d = get_itraq_ions(spec)
    diccio['itrq'] = ['%i' % a, '%i' % b, '%i' % c, '%i' % d]
#
#
def add_best_sequence(diccio, cutoff=0.4):
    """This function should not remove anything.
    Must be corrected para que sea add_two_best_sequences.
    Hacer el cutoff en compacta()
    """
    bestsec = []
    bestscore = []
    sequences = diccio['seq']
    scorelist = [float(score) for score in diccio['score']]
    sortedpairs = zip(scorelist, sequences)
    sortedpairs.sort(reverse=True)
    #
    try:
        bestsec.append(sortedpairs[0][1])
        bestscore.append('%.2f' % (sortedpairs[0][0]))
    except IndexError:
        diccio['bseq'] = ['*', '*']
        diccio['bscore'] = ['0', '0']
        return
    #
    try:
        segundo_score = sortedpairs[1][0]
    except IndexError:
        segundo_score = -1
    #    
    if segundo_score > cutoff:
        bestsec.append(sortedpairs[1][1])
        bestscore.append('%.2f' % (sortedpairs[1][0]))
    else:
        bestsec.append('*')
        bestscore.append('0')
    
    diccio['bseq'] = bestsec
    diccio['bscore'] = bestscore
#
#
def add_data(diccio, directory, scx):
    """Extract mass_p and charge de la filename en diccio['name']
    diccio[name] = "spectra/983.6667_2_1.ann"
    y anade estos valores junto con las keys 'scx' y 'file' al diccio
    Llamado desde:
        run_anz(frame, sourcelist, filtrar) -->
        preparar_data(source, propiedades)  -->
        add_data(diccio, directory, scx)
    """
    name = diccio['name']
    directory, filename = os.path.split(name[0])
    dat = filename.split('_')
    diccio['scx'] = [scx]
    diccio['file'] = [filename]
    try:
        diccio['mass_p'] = ['%.3f' % float(dat[0])]
        charge = dat[1].split('.')[0]
        diccio['charge'] = [charge]
    except IndexError:
        diccio['mass_p'] = []
        diccio['charge'] = []
#
#
def make_tags_from_pconf(diccio, cutoff=0.9):
    """For each proposed sequence (stored en seclist),
    PEAKS gives the goodness of the different locus or tags (en scoretxtlist).
    This function selects los mejores tags de cada sequence y los presenta
    en la forma ["tag1 | tag2 | ... | tagn"]
    El "try" protege la funcion select_goodtag porque hay files que da escores
    tipo "NaN". Esto se observa en files anz directas de raw con tienen espectros
    with only two signals: peaks propone de todas maneras una sequence pero
    entonces los scores salen extranos    
    """
    tags = []
    seclist = diccio['seq']
    scores_texts = diccio['pos_conf']
    #
    for sequence, scoretxt in zip(seclist, scores_texts):
        scores = scoretxt.split()
        #
        try:
            tags = select_goodtag(sequence, scores, cutoff)
            tags.extend(tags)   # new
        except ValueError:
            print "weird file: ", diccio

    tags = set(tags)     # new
    diccio['tag'] = [' | '.join(tags)]
#
#
def select_goodtag(secuencia, scorelist, cutoff=0.7):
    """"""
    item = 0
    newlist = []
    fltscorelist = [float(item) for item in scorelist]
    #
    sec = secuencia.replace('(CamC)', 'c')
    sec = sec.replace('(MetOxM)', 'm')
    sec = sec.replace('(iTRAQ', '')
    sec = sec.replace('(Acetyl', '')
    sec = sec.replace(')', '')
    #
    for item, score in zip(sec, fltscorelist):
        if score > cutoff:
            newlist.append(item)
        else:
            newlist.append('0')
    #
    text = ''.join(newlist)
    tags = text.split('0')
    tags = [tag for tag in tags if len(item) > 3]
    return tags

def get_itraq_ions(spectrum):
    """
    Sums the intensities around the mass of each itraq ion, los extremos
    valmxxx/valMxxx se definen globalmente 
    """
    int114 = int115 = int116 = int117 = 0
    #
    for intvalue, massvalue in spectrum:
        if massvalue < valm114:
            continue
        #
        if valm114 <= massvalue < valM114:
            int114 += intvalue
            continue
        if valm115 <= massvalue < valM115:
            int115 += intvalue
            continue
        if valm116 <= massvalue < valM116:
            int116 += intvalue
            continue
        if valm117 <= massvalue < valM117:
            int117 += intvalue
            continue
        #
        if massvalue >= valM117:
            break
        
    return int114, int115, int116, int117
#
#
if __name__ == "__main__":
   
    directory = 'directory'
    scx = 'scx'
    spec1 = "12 23\n24 48\n47 33\n114 120"
    spec2 = "11 22\n25 49\n46 34\n115 250"
    dictionaries = [
        {'name': ['spectra/888.66_2_1.ann'], 'peaks':[spec1], 'seq':['ASDF'],
         'score':[50], 'pos_conf':["0 0 0 0"]},
        {'name': ['spectra/999.66_2_1.ann'], 'peaks':[spec2], 'seq':['GFDK'],
         'score':[20], 'pos_conf':["0 0 0 0"]}
    ]
                
    set_global_itraq([0, 400])
    for item in get_full_data(dictionaries, directory, scx, 2, 10, 10):
        print item
        
##{'mass_p': ['888.660'], 'score': [50], 'seq': ['ASDF'], 'itrq': ['120', '0', '0', '0'],
## 'peaks': ['12 23\n24 48\n47 33\n114 120'], 'tag': [''], 'carga': ['2'],
## 'fpks': ['114.00', '24.00'], 'pos_conf': ['0 0 0 0'], 'scx': ['scx'],
## 'file': ['888.66_2_1.ann'], 'bseq': ['ASDF', '*'], 'bscore': ['50.00', '0'],
## 'name': ['spectra/888.66_2_1.ann']}
##{'mass_p': ['999.660'], 'score': [20], 'seq': ['GFDK'], 'itrq': ['0', '250', '0', '0'],
## 'peaks': ['11 22\n25 49\n46 34\n115 250'], 'tag': [''], 'carga': ['2'],
## 'fpks': ['115.00', '25.00'], 'pos_conf': ['0 0 0 0'], 'scx': ['scx'],
## 'file': ['999.66_2_1.ann'], 'bseq': ['GFDK', '*'], 'bscore': ['20.00', '0'],
## 'name': ['spectra/999.66_2_1.ann']}
##Script terminated.