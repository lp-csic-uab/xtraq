
---

**WARNING!**: This is the *Old* source-code repository for Xtraq program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/xtraq/) located at https://sourceforge.net/p/lp-csic-uab/xtraq/**  

---  
  
  
# Xtraq program

Xtraq extracts iTRAQ data from .ANZ Peaks results files.

---

**WARNING!**: This is the *Old* source-code repository for Xtraq program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/xtraq/) located at https://sourceforge.net/p/lp-csic-uab/xtraq/**  

---  
  

