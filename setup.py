# exWx/setup.py
#
## python setup.py py2exe 
#
from distutils.core import setup
import py2exe

setup(
    windows = [
              {'script': "xtraq_gui_36.pyw",
               'icon_resources':[(0,'images/xtraq.ico')]
              }
              ],
            
    options = {
              'py2exe': {
                        #'packages' :    [],
                        #'includes':     [],
                        'excludes':     ['matplotlib', 'email', 'testing', 'fft',
                                         'Tkconstants', 'Tkinter', 'tcl'
                                        ],
                        'ignores':       ['wxmsw26uh_vc.dll'],
                        'dll_excludes': ['libgdk_pixbuf-2.0-0.dll',
                                         'libgdk-win32-2.0-0.dll',
                                         'libgobject-2.0-0.dll'
                                        ],
                        'compressed': 1,
                        'optimize':2,
                        'bundle_files': 1
                        }
              },
    zipfile = None,
    data_files = [
                 ]
    )
