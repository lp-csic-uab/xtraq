#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
xtraq_warnings_32.py
(xtraq_36)
"""
#
import wx
#
#
error_fatal = (KeyboardInterrupt, MemoryError)
#
#
def aviso(avis, fatal=False):
    """popup warnings. Devolver un valor 0 es solo posible en el caso de
    aviso fatal al hacer "cancel".
    Sirve para relanzar el error (raise) para parar el programa
    """
    style = wx.OK|wx.ICON_EXCLAMATION
    if fatal: style = wx.OK|wx.ICON_WARNING|wx.CANCEL
    dlg = wx.MessageDialog(None, avis, "Esto es un aviso", style)
    result = dlg.ShowModal()
    if result == wx.ID_OK:
        dlg.Destroy()
        return 1
    elif result == wx.ID_CANCEL:
        dlg.Destroy()
        return 0
    
    
if __name__ == "__main__":
    
    App=wx.PySimpleApp()
    signal = aviso("aviso no fatal")
    print "aviso no fatal devolvio= ", signal
    signal = aviso("aviso fatal", fatal=True)
    print "aviso fatal devolvio= ", signal