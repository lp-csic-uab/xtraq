#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
xtraq_anzreader_32.py
(xtraq_36)
"""
#
import traceback
import zipfile as zipf
from cStringIO import StringIO
from xtraq_text import NOSEINCLUIRA
from xtraq_warnings import error_fatal, aviso
#
#
xmltags = ['SCAN', 'peaks', 'seq', 'score', 'pos_conf']
dictags = dict(SCAN=('<SCAN', '</SCAN>'),
               SCAN1=('<src_file', '</src_file>'),
               SCAN2=('<Merge_Scan', '</Merge_Scan>'),     # files M.Gorga january 2008
               peaks=('<peaks>', '</peaks>'),
               seq=('<seq>', '</seq>'),
               score=('<score>', '</score>'),
               pos_conf=('<pos_conf>', '</pos_conf>')
               )
#
#
def extract_anz(arch, totags):
    """
    anz archives have a spectra/ subdirectory
    """
    #
    types = [read_scan_type0, read_scan_tipo1, read_scan_tipo2]
    dicciolist = []
    tags = [key for key in totags if key != 'SCAN']
    #
    file_zip = zipf.ZipFile(arch, 'r')
    fnamelist = file_zip.namelist()
    #
    _type = get_anz_type(file_zip)
    f = types[_type]
    #
    for item in fnamelist:
        if not item.startswith('spectra/'):
            continue
        if not item.endswith('.ann'):
            continue
        try:
            dicciolist.append(extract_ann(file_zip, item, tags, f))
        except error_fatal:
            raise
        except Exception:
            f = StringIO()
            traceback.print_exc(file=f)
            valor = f.getvalue()
            aviso(valor)
            seguir = aviso(NOSEINCLUIRA % item, True)
            if not seguir:
                raise
    #
    return dicciolist
           
         
def extract_ann(fzip, ann, tags, file_lectura=None):
    """
    name    spectra/983.6667_2_1.ann
    SCAN    value [u'1814']
    peaks   value [u'70.072 1.2749484\n114.181 5.0157323\n115.215 1.3348994 [.......]\n1955.866 1.5450251\n1963.216 1.0752147\n']
    seq     value [u'YHWDTDLENDEFEPR', u'NWWDTDLENDEFEPR', u'NWWVVS(CamC)GANDEMNQR', u'WNWVVS(CamC)GANDEMNQR', u'YHWVVS(CamC)GANDEMNQR']
    score   value [u'0.5793749', u'0.36144528', u'0.01871173', u'0.013353186', u'0.011653193']
    pos_conf value [u'0.59617406 0.5961688 0.9995422 0.9408226 0.9408207 0.9408207 0.9408207 0.9408207 0.9998852
                    0.9998852 0.99991924 0.9408525 0.9408525 0.9413411 0.99992007 ', [... , ... , ...],
                    u'0.59617406 0.5961688 0.9995422 0.0 0.0 0.0 0.0 0.0 0.0 0.9998852 0.9998852 0.99991924 0.0 0.0 0.0 0.99992007 ']
    """
    diccio = {}
    diccio['name'] = [ann]
    xmlfile = fzip.read(ann)
    #
    #print "file", ann
    #curiosamente el print anterior da un error cuando se corre desde un
    #enlace a xtraq_gui_30.pyw.
    #no ocurre si se cambia a .py o si se hace desde SPE con o sin consola.
    #Falla aunque se ponga print "hola"
    #error -> IOError: [Errno 9]: Bad file descriptor
    #
    #print tags
    for key in tags:
        indx_ini = 0
        tagitemlist = []
        srchtgs = dictags[key]
        while True:
            start = xmlfile.find(srchtgs[0], indx_ini)
            if start == -1:
                break
            ini = xmlfile.find('>', start)
            end = xmlfile.find(srchtgs[1], ini)
            indx_ini = end
            tagitemlist.append(xmlfile[ini + 1:end])
        diccio[key] = tagitemlist
    #
    diccio['SCAN'] = [file_lectura(xmlfile)]
    #
    return diccio

def get_anz_type(fzip):
    """Determines anz type in order to extract the 'SCAN' value.

    Three types of file:
      ????? --> no se cuales son. Algo con lo que trabaje inicialmente
      PEAKS --> <src_file query_index="0">G:\50mM.662.662.2.dta</src_file>
      mrf   --> <Merge_Scan NumberOfMergedSpectra="1" begin_RT="17.61" end_RT="17.61">1057 </Merge_Scan>
    """
    _type = None
    fnamelist = fzip.namelist()
    keys = ['SCAN', 'SCAN1', 'SCAN2']
    #
    for item in fnamelist:
        if not item.startswith('spectra/'):
            print "invalid item", item
            continue
        if not item.endswith('.ann'):
            print "invalid item", item
            continue
        try:
            #print item
            xml_file = fzip.read(item)
            for key in keys:
                indx_ini = 0
                srchtgs = dictags[key]
                while True:
                    #print srchtgs
                    start = xml_file.find(srchtgs[0], indx_ini)
                    if start == -1:
                        break
                    ini = xml_file.find('>', start)
                    end = xml_file.find(srchtgs[1], ini)
                    indx_ini = end
                    value = xml_file[ini + 1:end]
                    _type = keys.index(key)
                    #print "es scan %s con %s ????" %(value, tipo)
                    if is_scan(value, _type):
                        return _type
        except error_fatal:
            raise
        except Exception:
            f = StringIO()
            traceback.print_exc(file=f)
            valor = f.getvalue()
            aviso(valor)
            go_on = aviso("FILE %s FAILED IN TYPE TEST" % item, True)
            if not go_on:
                raise
        
        return _type

def is_scan(valor, tipo):
    """
    Defines the working file type based on the location of the scan number
    assuring the correct value will be taken.
    #
    tipo 0 -> ???? --> do not remember. Algo con lo que trabaje inicialmente
                       <scan>1235</scan>
    tipo 1 -> PEAKS -> <src_file query_index="0">G:\50mM.662.662.2.dta</src_file> 
    tipo 2 -> mrf  --> <Merge_Scan NumberOfMergedSpectra="1" begin_RT="17.61" end_RT="17.61">1057 </Merge_Scan>
    """
    #
    if tipo == 1:
        valor = valor.split('.')[-3]
    elif tipo == 2:
        valor = valor.strip()
        
    return valor.isalnum()

def read_scan_type0(text):
    """Reads the scan value in <SCAN>number_scan</SCAN>
    """
    start = text.find('<SCAN', 0)
    if start == -1:
        return 0
    ini = text.find('>', start)
    end = text.find('</SCAN>', ini)
    
    return text[ini + 1:end]
        
def read_scan_tipo1(texto):
    """lee valor de scan en
    <src_file query_index='0'>G:\50mM.662.662.2.dta</src_file>
    """
    start = texto.find('<src_file', 0)
    if start == -1:
        return 0
    ini = texto.find('>', start)
    end = texto.find('</src_file>', ini)
    
    valor = texto[ini + 1:end]
    return valor.split('.')[-3]

def read_scan_tipo2(texto):
    """lee valor de scan en
    <Merge_Scan NumberOfMergedSpectra="1" begin_RT="17.61" end_RT="17.61">1057 </Merge_Scan>
    """
    start = texto.find('<Merge_Scan', 0)
    if start == -1:
        return 0
    ini = texto.find('>', start)
    end = texto.find('</Merge_Scan>', ini)
    
    valor = texto[ini + 1:end]
    return valor.strip()

if __name__ == "__main__":
    
    #archivo_de_prueba = "50test.anz"
    arch = "gorga_mini.anz"
  
    dicciolist = extract_anz(arch, xmltags)
    diccio = dicciolist[0]
    
    for key, value in diccio.iteritems():
        print ''
        if key == 'peaks':
            text = value[0]
            valtext = '\n' + text[0:100] + '\n........\n' + text[-100:]
            value = [valtext]
        for val in value:
            print "%s  ---> %s" %(key, val) 