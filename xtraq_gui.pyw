#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
xtraq_gui_37
(xtraq_37)
Este modulo siempre sube a la version mas alta
"""
##C:\50test.anz (XML parsing, psyco)
##tiempo get_no_itraq  8.20
##.... tiempo extract_anz  7.87
##tiempo borra de anz  0.02
##tiempo save_anz      0.16
##               total 8.38
##guardados 2, filtrados 43
##
####    FILTRADO      ####
##C:\XANZ\50bws.anz
##   #  Portatil(psyco) #             # PC trabajo(terminal, psyco) #
##C:\XANZ\50bws.anz (psyco)               vs3.0    vs3.2    vs3.3    vs3.4   vs3.5
##tiempo get_no_itraq   5.89            # 3.08   # 3.17   # 3.05   # 3.06  # 3.05
##.... tiempo extract_anz   0.74        #  0.25  #  0.23  #  0.22  #  0.24 #  0.25
##.... tiempo get_spec      5.008 **    #  2.83  #  2.92  #  2.76  #  2.83 #  2.78 ****
##.... tiempo get_itraq     0.03        #  0.00  #  0.00  #  0.06  #  0.00 #  0.00
##tiempo borra de anz   1.12            # 3.86   # 2.56   # Thrd   # Thrd  # Thrd
##tiempo save_anz       10.04    ****   # 0.97   # 1.19   # Thrd   # Thrd  # Thrd
##               total  17.05           # 7.91   # 6.92   # 3.05+t # 6.06  # 3.06
##guardados 474, filtrados 526
## vs3.3 archiva los datos con threads. No cuenta bien "guardados" todavia
## vs3.5 archiva directamente sin directorios intermedios!!!!
##
#######    REPORT      ######
##C:\XANZ\50pks.anz 
##   #  Portatil(psyco) #             # PC trabajo(terminal, psyco) #
##C:\XANZ\50pks.anz (psyco)             vs3.0   vs3.2  vs3.3
##tiempo preparar_data  11.23   ****    # 5.48 # 5.50 # 5.39
##tiempo generar_xls     0.18           # 0.25 # 0.22 # 0.20
##tiempo save_xls        0.01           # 0.03 # 0.00 # 0.00
##                total 11.42           # 5.77 # 5.72 # 5.59
#
import wx
import os
import threading
from xtraq_class_main import MyFrame
from xtraq_class_iso import IsoFrame
from xtraq_class_cal import CaliFrame
from xtraq_class_spc import SpectFrame
from xtraq_ommani import ManiClock
from xtraq_warnings import aviso
from xtraq import run
from xtraq_text import CABECERA, ESPECTRO, INTRO
#
#
XTRAQ_TITULO = "  THE ITRAQ HACKER 3.6"
#
#FILETEST = r'C:\XTRAQ\50mM100c.anz'
FILETEST = r'C:\XTRAQ\50pks.anz'
RESULTEST = r'C:\XTRAQ\50bws_r.anz'
EXCELFILE = r'C:\XTRAQ\50xls_r.xls'
INDEXFILE = r'C:\XTRAQ\index.idx'
#
def_dir = r'C:\XTRAQ'
#
WILDCARD_IO = "Peaks files (*.anz)|*.anz| all (*.*)|*.*"
#
#           R114   R115    R116    R117
iso_data = [[1.000, 0.020, 0.000, 0.000],   # 114
           [0.063, 1.000, 0.030, 0.000],    # 115
           [0.002, 0.060, 1.000, 0.040],    # 116
           [0.000, 0.001, 0.049, 1.000]     # 117
            ]
#
cali_data = (0, 450)  # (center, window)
#
specdata = dict(mthd=0,
                total=4,
                min=3,
                par=300,
                frg=400
                )               # data spectra analysis
#
class CaliTraq(CaliFrame):
    def __init__(self, parent, calidata, *args, **kargs):
        CaliFrame.__init__(self, parent, *args, **kargs)
        self.label_1.SetValue(CABECERA)
        self.parent = parent
        self.set_data(calidata)
        self.Bind(wx.EVT_BUTTON, self.on_bt_apply, self.bt_apply)
        self.Bind(wx.EVT_BUTTON, self.on_bt_exit, self.bt_exit)
        
    def set_data(self, data):
        self.sp_cntr.SetValue(data[0])
        self.sp_wndw.SetValue(data[1])
    
    def get_data(self):
        center = self.sp_cntr.GetValue()
        window = self.sp_wndw.GetValue()
        return center, window
        
    def on_bt_exit(self, evt):
        self.Destroy()
    
    def on_bt_apply(self, evt):
        data = self.get_data()
        self.parent.property['calidata'] = data
#
#
class IsoTraq(IsoFrame):
    def __init__(self, parent, isodata, title, *args, **kargs):
        IsoFrame.__init__(self, parent, *args, **kargs)
        self.parent = parent
        self.set_data(isodata)
        self.SetTitle(title)
        self.Bind(wx.EVT_BUTTON, self.on_bt_apply, self.bt_apply)
        self.Bind(wx.EVT_BUTTON, self.on_bt_exit, self.bt_exit)
        self.Bind(wx.EVT_BUTTON, self.on_bt_save, self.bt_save)
        self.Bind(wx.EVT_BUTTON, self.on_bt_load, self.bt_load)
        self.Bind(wx.EVT_BUTTON, self.on_bt_svdflt, self.bt_svdflt)
    
    def set_data(self, data):
        for row in range(4):
            for column in range(4):
                self.grd.SetCellValue(row, column, '%.3f' % data[row][column])
    
    def get_data(self):
        data = []
        for row in range(4):
            rowl = []
            for column in range(4):
                rowl.append(float(self.grd.GetCellValue(row, column)))
            data.append(rowl)
        return data
    
    def on_bt_exit(self, evt):
        self.Destroy()
     
    def on_bt_apply(self, evt):
        """"""
        data = self.get_data()
        self.parent.property['isodata'] = data
        title = self.GetTitle()
        if 'CLICK' in title:
            title = title[0:-13]
        self.SetTitle(title)
        
    def on_bt_svdflt(self, evt):
        """"""
        save_path = def_dir + os.sep + 'default'
        self.save(save_path)
        title = "ISOTOPE TABLE -- %s  (CLICK APPLY)" % 'default'
        self.SetTitle(title)
    
    def on_bt_save(self, evt):
        """"""
        save_path = def_dir
        #
        dlg = wx.FileDialog(None, "Select File",
                            defaultDir=self.parent.sdef_dir,
                            style=wx.SAVE|wx.OVERWRITE_PROMPT)
        #
        if dlg.ShowModal() == wx.ID_OK:
            save_path = dlg.GetPath()
            self.save(save_path)
            
    def save(self, savepath):
        """"""
        dummmy, filename = os.path.split(savepath)
        data = self.get_data()
        text = ''
        for item in data:
            txtlist = [str(number) for number in item]
            text += '\t'.join(txtlist) + '\n'
        open(savepath, 'w').write(text)
        self.parent.sdef_dir = savepath
        title = "ISOTOPE TABLE -- %s  (CLICK APPLY)" % filename
        self.SetTitle(title)
        
    def on_bt_load(self, evt):
        """"""
        dlg = wx.FileDialog(None, "Select File",
                            defaultDir=self.parent.sdef_dir,
                            style=wx.SAVE)
        
        if dlg.ShowModal() == wx.ID_OK:
            loadpath = dlg.GetPath()
            #
            data = []
            for line in open(loadpath, 'r'):
                row = line.split()
                row = [float(item) for item in row]
                data.append(row)
            self.set_data(data)
            self.parent.sdef_dir = loadpath
            dummy, filename = os.path.split(loadpath)
            title = "ISOTOPE TABLE -- %s   (CLICK APPLY)" % filename
            self.SetTitle(title)
#
#
class SpecTraq(SpectFrame):
    def __init__(self, parent, specdata, *args, **kargs):
        SpectFrame.__init__(self, parent, *args, **kargs)
        self.parent = parent
        self.tc_main.SetValue(ESPECTRO)
        self.setdata(specdata)
        self.Bind(wx.EVT_BUTTON, self.on_bt_apply, self.bt_apply)
        self.Bind(wx.EVT_BUTTON, self.on_bt_exit, self.bt_exit)
        
    def setdata(self, data):
        self.rbx_mth.SetSelection(data['mthd'])
        self.sp_tot.SetValue(data['total'])
        self.sp_min.SetValue(data['min'])
        self.sp_par.SetValue(data['par'])
        self.sp_frg.SetValue(data['frg'])
    
    def getdata(self):
        data = {}
        data['mthd'] = self.rbx_mth.GetSelection()
        data['total'] = self.sp_tot.GetValue()
        data['min'] = self.sp_min.GetValue()
        data['par'] = self.sp_par.GetValue()
        data['frg'] = self.sp_frg.GetValue()
        
        return data
        
    def on_bt_exit(self, evt):
        self.Destroy()
    
    def on_bt_apply(self, evt):
        data = self.getdata()
        if data['total'] < data['min']:
            aviso("the number of matched can not be higher than total")
            data['min'] = data['total']
            self.sp_min.SetValue(data['min'])
        self.parent.property['specdata'] = data
        self.parent.statusbar.SetStatusText(str(data), 0)
#
#
class Xtraq(MyFrame):
    def __init__(self, *args, **kargs):
        MyFrame.__init__(self, *args, **kargs)
        #
        try:
            isodatax = self.lee_isodata()
            self.title = 'default'
        except IOError:
            isodatax = iso_data
            self.title = 'ref interna'
        #
        self.maniclock = None
        self.fidx = None
        #
        self.property['isodata'] = isodatax
        self.property['calidata'] = cali_data
        self.property['specdata'] = specdata
        #
        self.tc_main.SetValue(INTRO)
        #
        if os.path.exists(FILETEST):
            filetest = FILETEST
            resultest = RESULTEST
            excelfile = EXCELFILE
            indexfile = INDEXFILE
        else:
            directory, dummy = os.path.split(FILETEST)
            filetest = ''
            if os.path.exists(directory):
                resultest = RESULTEST
                excelfile = EXCELFILE
                indexfile = INDEXFILE
            else:
                resultest = ''
                excelfile = ''
                indexfile = ''
        #
        self.source = filetest
        self.target = resultest
        self.excel = excelfile
        self.index = indexfile
        #
        self.tc_flin.SetValue(filetest)
        self.tc_flout.SetValue(resultest)
        self.tc_fxls.SetValue(excelfile)
        self.tc_fidx.SetValue(indexfile)        
        #
        self.idef_dir = def_dir
        self.odef_dir = def_dir
        self.xdef_dir = def_dir
        self.sdef_dir = def_dir
        #
        if os.path.exists(self.excel):
            self.bt_siit.SetBackgroundColour("green")
        #
        self.SetTitle(XTRAQ_TITULO)
        self.tc_cffitraq.SetValue('100')
        self.tc_cffscr.SetValue('0.5')
        self.tc_cfftgs.SetValue('0.9')
        #
        self.show_text_entries()
        self.show_report_params('anz')
        # 
        self.Bind(wx.EVT_BUTTON, self.on_run, self.bt_run)
        self.Bind(wx.EVT_BUTTON, self.on_flin, self.bt_flin)
        self.Bind(wx.EVT_BUTTON, self.on_flout, self.bt_flout)
        self.Bind(wx.EVT_BUTTON, self.on_fxls, self.bt_fxls)
        self.Bind(wx.EVT_BUTTON, self.on_bt_siit, self.bt_siit)
        self.Bind(wx.EVT_RADIOBOX, self.on_rbx_op, self.rbx_op)
        self.Bind(wx.EVT_RADIOBOX, self.on_rbx_tp, self.rbx_tp)
        self.Bind(wx.EVT_CHECKBOX, self.on_cbx_bcht, self.cbx_bcht)
        self.Bind(wx.EVT_CLOSE, self.on_close_window)
        self.Bind(wx.EVT_BUTTON, self.on_bt_iso, self.bt_iso)
        self.Bind(wx.EVT_BUTTON, self.on_bt_cal, self.bt_cal)
        self.Bind(wx.EVT_BUTTON, self.on_bt_spec, self.bt_spec)
        self.Bind(wx.EVT_BUTTON, self.on_fidx, self.bt_fidx)
        
    def lee_isodata(self):
        """Reads file 'default' in folder 'def_dir'"""
        data = []
        for line in open(def_dir + os.sep + 'default', 'r'):
            row = line.split()
            row = [float(item) for item in row]
            data.append(row)
        return data
        
    def revisa(self, arch, direct):
        """"""
        if arch.startswith('..\\'):
            return direct + os.sep + os.path.split(arch)[-1]
        else:
            return arch
     
    def on_run(self, event):   # wxGlade: MyFrame.<event_handler>
        """"""
        self.getframeparams()
        flin = self.property['flin']
        posx, posy = self.GetPosition()
        position = posx + 16, posy + 35
        self.maniclock = ManiClock(None, title=" OmClock", pos=position)
        self.maniclock.Show()
        #
        self.source = self.revisa(flin, self.idef_dir)
            
        if not os.path.exists(self.source):
            aviso('no existe la file de entrada')
        else:
            flout = self.property['flout']
            fxls = self.property['fxls']
            self.target = self.revisa(flout, self.odef_dir)
            self.excel = self.revisa(fxls, self.xdef_dir)
            tarea = threading.Thread(target=run, args=(self,))
            tarea.start()
            
    def on_flin(self, event):
        """"""
        if self.property['bcht'] or not self.property['tp']:
            dlg = wx.DirDialog(None, "Select Dir", defaultPath=self.idef_dir, style=wx.OPEN)
        else:
            dlg = wx.FileDialog(None, "Select File", defaultDir=self.idef_dir,
                                wildcard=WILDCARD_IO, style=wx.OPEN)
        #
        if dlg.ShowModal() == wx.ID_OK:
            self.source = dlg.GetPath()
            shortcut = self.get_shortcut(self.source)
            self.idef_dir = os.path.dirname(self.source)
            self.tc_flin.SetValue(shortcut)
    
    def get_shortcut(self, prop):
        """"""
        path_list = prop.split('\\')
        if len(path_list) > 2:
            return r"..\\" + path_list[-2] + os.sep + path_list[-1]
        else:
            return prop
        
    def on_flout(self, event):
        """"""
        if self.property['bcht'] or not self.property['tp']:
            dlg = wx.DirDialog(self, "Select Dir", defaultPath=self.odef_dir,
                               style=wx.SAVE|wx.OVERWRITE_PROMPT)
        else:
            dlg = wx.FileDialog(self, "Select File", defaultDir=self.odef_dir,
                                wildcard=WILDCARD_IO,
                                style=wx.SAVE | wx.OVERWRITE_PROMPT)
        #
        if dlg.ShowModal() == wx.ID_OK:
            self.target = dlg.GetPath()
            shortcut = self.get_shortcut(self.target)
            self.odef_dir = os.path.dirname(self.target)
            self.tc_flout.SetValue(shortcut)
        
    def on_fxls(self, event):   # wxGlade: MyFrame.<event_handler>
        ""
        wildcard = "Excel (tab separated)(.xls)|*.xls|All(*.*)|*.*"
        dlg = wx.FileDialog(self, "Select File", defaultDir=self.xdef_dir,
                            wildcard=wildcard,
                            style=wx.SAVE | wx.OVERWRITE_PROMPT)
        
        if dlg.ShowModal() == wx.ID_OK:
            self.excel = dlg.GetPath()
            #
            if os.path.exists(self.excel):
                self.bt_siit.SetBackgroundColour("green")
            else:
                self.bt_siit.SetBackgroundColour("red")
            #
            shortcut = self.get_shortcut(self.excel)
            self.xdef_dir = os.path.dirname(self.excel)
            self.tc_fxls.SetValue(shortcut)
    
    def on_fidx(self, event):  # wxGlade: MyFrame.<event_handler>
        """"""
        wildcard = "indices(.idx)|*.idx|All(*.*)|*.*"
        dlg = wx.FileDialog(self, "Select File", defaultDir=self.xdef_dir,
                            wildcard=wildcard,
                            style=wx.SAVE|wx.OVERWRITE_PROMPT)
        
        if dlg.ShowModal() == wx.ID_OK:
            self.fidx = dlg.GetPath()
            #
            shortcut = self.get_shortcut(self.fidx)
            self.tc_fidx.SetValue(shortcut)
    
    def on_bt_siit(self, evt):
        """open excel"""
        self.getframeparams()
        fxls = self.property['fxls']
        self.excel = self.revisa(fxls, self.xdef_dir)
        if os.path.exists(self.excel):
            self.bt_siit.SetBackgroundColour("green")
            texto = "Reading from file %s" % self.excel
            self.statusbar.SetStatusText(texto, 0)
            os.startfile(self.excel)
        else:
            texto = "La file %s no existe o da error de carga" % self.excel 
            self.statusbar.SetStatusText(texto, 0)
            self.bt_siit.SetBackgroundColour("red")
        
    def on_rbx_tp(self, evt):
        """"""
        tpselected = self.rbx_tp.GetSelection()
        self.property['tp'] = tpselected
        if not self.property['tp']:  # si .dta
            self.property['op'] = 1
            self.rbx_op.SetSelection(1)
            self.show_text_entries()
            self.show_report_params('dta')
        else:
            self.property['op'] = self.rbx_op.GetSelection()
            self.show_text_entries()
            self.show_report_params('anz')
            
    def on_rbx_op(self, evt):
        """"""
        self.property['op'] = self.rbx_op.GetSelection()
        self.show_text_entries()
        self.show_report_params('anz')
    
    def on_bt_iso(self, evt):
        """
        Read the isotopic distribution of the reagents for quantification.
        """
        title = "ISOTOPE TABLE -- %s" % self.title
        isoframe = IsoTraq(self, self.property['isodata'], title)
        isoframe.Show()
        
    def on_bt_cal(self, evt):
        """Read the center of the iTRAQ ions and the acceptable deviation"""
        cali_frame = CaliTraq(self, self.property['calidata'])
        cali_frame.Show()
        
    def on_bt_spec(self, evt):
        """
        Opens the window to adjust the number of ions and spectra comparison.
        """
        spec_frame = SpecTraq(self, self.property['specdata'])
        spec_frame.Show()
    
    def show_text_entries(self):
        """"""
        FILTRAR = DTA = 0
        REPORT = ANZ = 1
        check = False
        lista = [0, 0,
                 1, 1,
                 1,
                 0, 0,
                 1,
                 1, 1,
                 0  ]
        #
        tpselected = self.property['tp']
        opselected = self.property['op']
        if tpselected == DTA:
            pass            
            #
        if tpselected == ANZ:
            check = True
            if opselected == FILTRAR:
                lista = [1, 1,
                         0, 0,
                         1,
                         0, 0,
                         0,
                         0, 0,
                         1  ]
            if opselected == REPORT:
                lista = [0, 0,
                         1, 1,
                         0,
                         1, 1,
                         1,
                         1, 1,
                         1  ]
        #
        self.cbx_bseq.SetValue(check)
        self.cbx_bscr.SetValue(check)
        self.cbx_pktgs.SetValue(check)
        self.cbx_xprt.SetValue(check)
        #
        self.switch_on(lista)
        
    def switch_on(self, lista):
        """"""
        cbxs = [self.tc_flout.Enable, self.bt_flout.Enable,
                self.tc_fxls.Enable, self.bt_fxls.Enable,
                self.tc_cffitraq.Enable,
                self.tc_cffscr.Enable, self.tc_cfftgs.Enable,
                self.cbx_idx.Enable,
                self.tc_fidx.Enable, self.bt_fidx.Enable,
                self.rbx_op.Enable
                ]
        for function, state in zip(cbxs, lista):
            function(state)
    
    def show_report_params(self, group):
        """"""
        if group == 'anz':
            report_params = ['scx', 'bseq', 'scn', 'bscr', 'mp', 'pktgs',
                             'chg', 'fpks', 'ref', 'itrq', 'quant', 'xprt']
        else:
            report_params = ['scx', 'scn', 'mp', 'chg', 'fpks', 'ref', 'itrq', 'quant']
            self.cbx_bseq.Enable(False)
            self.cbx_bscr.Enable(False)
            self.cbx_pktgs.Enable(False)
            self.cbx_xprt.Enable(False)
        
        function = 'Enable'
        switch_on = self.property['op']
        #
        for objeto in report_params:
            name = 'cbx_' + objeto
            item = getattr(self, name)
            getattr(item, function)(switch_on)
            getattr(item, 'Refresh')()
    
    def printa(self):
        print 'hello'
        print self.property['calidata']
    
    def on_cbx_bcht(self, evt):
        bchtselected = self.cbx_bcht.IsChecked()
        self.property['bcht'] = bchtselected
                   
    def on_close_window(self, evt):
        try:
            self.maniclock.Destroy()
        except AttributeError:
            pass
        self.Destroy()

# end of class MyFrame

class MyApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        frame_1 = Xtraq(None, -1, "")
        self.SetTopWindow(frame_1)
        frame_1.Show()
        return 1

if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()
