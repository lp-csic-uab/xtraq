#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
# generated by wxGlade 0.5.1cvs on Thu Aug 16 13:20:06 2007
"""
xtraq_class_iso_05.py
(xtraq_37)
"""
#
import wx
from wx.grid import Grid
#
#
class IsoFrame(wx.Frame):
    def __init__(self, *args, **kwds):
        # begin wxGlade: MyFrame.__init__
        kwds["style"] = wx.CAPTION|wx.SYSTEM_MENU|wx.CLOSE_BOX|wx.FRAME_TOOL_WINDOW
        wx.Frame.__init__(self, *args, **kwds)
        self.lb_ion = wx.StaticText(self, -1, "\t\t  % REAGENT  ISOTOPIC  DISTRIBUTION",
                                    style=wx.ALIGN_CENTRE)
        self.lb_ion.SetBackgroundColour(wx.Colour(255, 255, 0))
        self.grd = Grid(self, -1, size=(10, 125)) 
        size = (40, 25)
        self.bt_apply = wx.Button(self, -1, "APPLY", size=size)
        self.bt_exit = wx.Button(self, -1, "EXIT", size=size)
        self.bt_load = wx.Button(self, -1, "LOAD", size=size)
        self.bt_save = wx.Button(self, -1, "SAVE", size=size)
        self.bt_svdflt = wx.Button(self, -1, "AS DEFAULT", size=size)
        
        self.__set_properties()
        self.__do_layout()
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: MyFrame.__set_properties
        self.SetTitle("ISOTOPE TABLE")
        self.SetMinSize((330, 10))
        self.SetBackgroundColour("yellow")
        self.grd.CreateGrid(4, 4)
        self.grd.EnableDragColSize(0)
        self.grd.EnableDragRowSize(0)
        self.grd.EnableDragGridSize(0)
        self.grd.SetRowLabelValue(0, "114")
        self.grd.SetColLabelValue(0, "R-114")
        self.grd.SetColSize(0, 50)
        self.grd.SetRowLabelValue(1, "115")
        self.grd.SetColLabelValue(1, "R-115")
        self.grd.SetColSize(1, 50)
        self.grd.SetRowLabelValue(2, "116")
        self.grd.SetColLabelValue(2, "R-116")
        self.grd.SetColSize(2, 50)
        self.grd.SetRowLabelValue(3, "117")
        self.grd.SetColLabelValue(3, "R-117")
        self.grd.SetColSize(3, 50)
        # end wxGlade

    def __do_layout(self):
        #
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_1.Add(self.lb_ion, 0, wx.EXPAND, 0)
        sizer_1.Add(self.grd, 1, wx.EXPAND|wx.LEFT|wx.RIGHT|wx.BOTTOM, 10)
        sizer_2.Add(self.bt_apply, 1, 0, 0)
        sizer_2.Add(self.bt_exit, 1, 0, 0)
        sizer_2.Add(self.bt_load, 1, 0, 0)
        sizer_2.Add(self.bt_save, 1, 0, 0)
        sizer_2.Add(self.bt_svdflt, 1, 0, 0)
        sizer_1.Add(sizer_2, 0, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        sizer_1.Fit(self)
        self.Layout()

class MyIsoApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        frame = IsoFrame(None, -1, "")
        self.SetTopWindow(frame)
        frame.Show()
        return 1

if __name__ == "__main__":
    isoapp = MyIsoApp(0)
    isoapp.MainLoop()
