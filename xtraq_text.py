#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
xtraq_text_07.py
(xtraq_36)
"""

INTRO =\
"""
                                                THE ITRAQ HACKER
                                                        vs_3.6
                                                J.Abian Abril 2009
                                    
Este programa extrae los datos de LC/MS/MS y MDLC/MS/MS de un experimento iTRAQ.

                                                    FUNCIONES
    - FUNCION FILTRADO
        -> Genera archivos XML comprimidos (.anz) a partir de
        archivos .anz de PEAKS donde se han eliminado los espectros sin se�ales
        de iones iTRAQ
    
    - FUNCION REPORT 
        -> Genera reports con datos extraidos de archivos .anz
       (usables con excel).
        Datos incluibles en el report:
            * Datos generales del espectro (file, scan, masa, carga)
            * Numero de Referencia asignado (ver mas abajo) 
            * Cuatro Iones mayoritarios
            * Marca de espectro unico/repetido evaluado en funcion de la masa
                del precursor y de los 4 iones mayoritarios.
            * Las dos mejores secuencias asignadas de novo por PEAKS (y sus scores).
                Si ningun candidato supera la probabilidad minima se indica la
                secuencia de mayor probabilidad.
            * Los tags de mayor probabilidad extraidos de las secuencias de novo
                de PEAKS en base al codigo de colores.
                Se incluyen todas las secuencias de longuitud mayor de 3 donde 
                todos los aminoacidos superen el score minimo.
            * Intensidad de iones iTRAQ (no procesada)
            * Intensidad de iones iTRAQ corregida.
    
        -> Genera reports similares a partir de directorios con archivos .dta. 
        Los datos incluibles se reducen a la informacion disponible en estos archivos.
        
        -> Tambien trabaja con anz de raw sin pasar por bioworks (datos rata Albert)
        Los fallos derivados de files con muy pocos fragmentos ya se han corregido
        
        -> Tambien trabaja con anz de mgf (metedura de pata de Gorga). Aunque no
        hubiera pasado nada si nos hubieramos ahorrado el trabajo.
        
        >>>>>>>>>>>>>>>>>>>>>NON PREOCUPARSI<<<<<<<<<<<<<<<<<<<
        >>>>>EL PROGRAMA DETECTA EL TIPO DE ARCHIVO DE ORIGEN EN LA ANZ<<<<<
        >>>>>>>>>>>>>>>>>Y LA PROCESA EN CONSECUENCIA<<<<<<<<<<<<<<
        
                    
                                 PROCEDIMIENTO GENERAL DE TRABAJO (ANZs)

        1. Los datos se procesan con Bioworks para generar un archivo (.srf)
        2. Exportar datos Biowors a PEAKS:
            - Se abre la busqueda en BioWorks
            - Se seleccionan todas las DTAs
                    * raton sobre listado -> boton derecho -> "select all"
            - "Export DTAs to PEAKS".
                    * Peaks debe estar abierto y vacio.
                    * Pregunta un directorio de almacenamiento de las DTAs.
                        + Seleccionar G:\ o un directorio creado dentro de G:\.
                            :: Recordar borrar este directorio periodicamente.
                    * En PEAKS, las DTAs aparecen bajo "Peptide data", en un
                        archivo sin nombre.
         
        3. Guardar archivo PEAKS generado:
            - Selecciona el nodo del grupo, no las DTAs individuales,
            - "Save as" como anz.
            - Para generar un nuevo .anz (p.e. otro salto de sal), reiniciar PEAKS.
        
        4. Filtrar eliminado scans sin datos ITRAQ 
           (Si se quiere trabajar sin filtrar pasar directamente al punto 5)
            - Cargar archivo/directorio anz en programa Xtraq
            - Indicar nombre archivo/directorio de salida
            - Seleccionar cutoff iTraq
            - RUN
            - Se genera un archivo/directorio anz reducido
    
        5. Utilizar el archivo anz filtrado para hacer busqueda de Novo con PEAKS
            - Para busqueda en bacht:
                    * Seleccionan los archivos .anz
                    * En "Data refine" --> NO a todo
                    * Posibles beneficios activando "Preprocess MS/MS"
                    * Se activa Auto de novo, Database search...
                    * Seleccionar la opcion "Close after saving"
                    * En la parte inferior aparecen los nombres de los .anz usados
                        y los nuevos; deben ser los mismos;
                    * Al lanzar la busqueda pregunta:
                        "Do you want to overwrite it?" --> "yes to all".
        
        6. Generar REPORT a partir del archivo .anz generado en 5.
            - Cargar archivo/directorio anz en programa Xtraq
            - Indicar nombre archivo xls de salida
            - Seleccionar cutoff score de secuencia y de tags
            - Seleccionar datos que queremos tener en el report
            - RUN


                                            TO-DO
        
        - Hay algun parser XML mas rapido que la busqueda directa en texto ???
        - Incluir visor XML o visor arbol XML y visor dta
        - Automatizar chequeo de calibrado de iones itraq?
        - Corregir valor correcto ion 114.1
        - A�adir ToolTips
        - Convertir este TxtCtrl en algo hojeable:
                    INTRO + FUNCIONES + PROCEDIMIENTO + TODO
        - Mas?



                                                NOTAS

1) Lo archivos ANZ pueden generarse a partir de archivos RAW o de colecciones DTA.
	Las principales diferencias al usar estos archivos son:

RAW ----> ANZ -- peaks dnv search ----> ANZ            SRF -->DTA --> ANZ --peaks dnv search--> ANZ
	
Nombre ANN = 984.0_2_1.ann                                         Nombre ANN = 1111.4015_3.ann
SI aparece tag <SCAN> en las ANN	                         NO aparece tag <SCAN> en las ANN
Cabecera tipica:                                                              Cabecera tipica:

<PeptideItem>                                                                   <PeptideItem>
             984.0 2 &lt;rt104.2162m&gt;                                   1111.4015 3
            <Instrument>Ion Trap</Instrument>                         <mass_spectrum version="1.0">
            <SCAN RT="104.2162">2534</SCAN>                    <src_file query_index="0">
            <mass_spectrum version="1.0">                                           ..\\50mM.2202.2202.3.dta</src_file>
            <src_file query_index="2302">
                     ..\\50mM_prueba.RAW</src_file>

PEAKS determina (mal) la carga de cada espectro  	Bioworks prepara DTAs con todas las cargas posibles
	

2) Cuando se hace busqueda de novo con peaks en base de datos el archivo final incluye el candidato
  de la base de datos como otro tag <seq> y con su correspondiente tag <score>.
   Con el programa XTRAQ actual es indistinguible de los de novo.
   Es posible que esto cree algun conflicto.
   Posible Solucion:
   Esta secuencia tiene el tag especifico        ------>        <src_program>PEAKS DB Search...
   Mientras que las sequencias de novo tienen -->       <src_program>PEAKS Auto De Novo...
   Ademas, esta es la clave!!, las secuencias de database no tienen tag <pos_conf>

3) Han aparecido errores extranos debido a files que tienen tags raros como:
      <score />
   complementados con datos como:
      <pos_conf>1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 0.0 0.0 0.0 0.0 1.0 1.0 1.0 </pos_conf>
   Ver carpeta ERROR
   Ahora el tag buscado es <score> lo que elimina el error.
   OJO!

4) El calculo de intensidad coregida de los iones itrAQ se lleva a cabo mediante
    la resolucion con algebra lineal del sistema de ecuaciones:
            Ia = a*faa + b*fba + c*fca + d*fda
            Ib = a*fab + b*fbb + c*fcb + d*fdb
            Ic = a*fac + b*fbc + c*fcc + d*fdc
            Id = a*fad + b*fbd + c*fcd + d*fdd
    donde
        Ii es la intensidad observada del ion i
        fij es la contribucion del reactivo i al ion j
        a,b,c,d la intensidad asignable a cada ion
    
    Aunque el procedimiento es limpisimo, en situaciones reales se generan
    numeros negativos mientras que para otros iones aumenta su valor corregido
    frente al observado. Esto ocurre porque:
      * asumimos que lo que vemos es iTRAQ lo cual no tiene porque ser verdad.
        De hecho, con un sistema preciso de medicion, la intensidad de la se�al
        isotopica determinaria el valor limite maximo del correspondiente
        ion itraq.
      * efecto de threshold pe 100/1 (mala relacion) vs 105/6 (correcto 6%).
      * error de la medida del espectrometro.
            - error muestreo normal, ruido
            - error debido a mala deteccion/integracion de picos peque�os a la
              izquierda de se�ales grandes (un viejo conocido).
              Por ejemplo en un archivo con mil espectros filtrados
             (50pks.anz, cutoff 100), con marcaje itrq 114 y 117,
              el numero de espectros con se�al 0 fue
                    114 ->   0
                    115 ->  31
                    116 -> 367
                    117 ->   0
              El mismo archivo con cutoff=1000 (480 espectros) dio
                    114 ->   0
                    115 ->  10
                    116 -> 177
                    117 ->   0
              Este resultado no confirma ? este fenomeno. Quizas 100 ya sea alto
    
    No se todavia como solucionar este problema
    
5) A partir de la version 2.6 el programa hace dos cosas mas:
    - compacta los reports eliminando datos multiples de carga +1, +2 y +3
    que son necesarios para la busqueda en Bioworks? pero que sobran en un
    report. Actualmente lo hace solo con anz pero no con dtas. 
    - da una referencia distinta a cada espectro distinto.
    dos espectros son distintos si el precursor difiere menos de ->0.3 uma
    y si mas de 2 de los 4 fragmentos coinciden con una precision -> 0.4 uma
    El procedimiento de comparacion no es muy eficiente y se ha preferido que
    sea estricto a que mezcle espectros diferentes.
    El programa permite guardar un indice XXX.idx en formato Pickle
    que guarda para cada numero de referencia la masa del precursor y de los
    fragmentos y que permite dar referencias entre archivos distintos pasados
    por separado. 
     
    
    
Nota final: editar en Times New Roman! Buscar cual es la correcta!!!Arrrggg!!!
""" 
#
DESPISTADO =\
"""
  Tienes el Excel abierto???
          ***
    Has tenido suerte:
Busca excel despistado en C:\\
"""
#
NOSEINCLUIRA =\
"""
   Error en %s
   No te preocupes: tu sigue
Esta file no se incluira en los datos
  (En el caso que debiera)
"""
#
CABECERA = """\
AJUSTE CALIBRADO IONES ITRAQ
      (mmu)
    
  Referencia Reactivo 114
      114.2

  valores ajuste defecto
desvio referencia --> 0 mmu
ancho de ventana -->450 mmu
"""
ESPECTRO = """\
PARAMETROS PARA SELECCION DE ESPECTROS
  Y GENERACION DE REFERENCIAS
      
    Ojo, por el momento, de los dos metodos,
solo funciona la comparacion simple de los iones
"""


#
if __name__ == "__main__":   
    print INTRO
    print DESPISTADO
    print NOSEINCLUIRA
    print CABECERA