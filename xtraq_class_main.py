#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
# generated by wxGlade 0.5.1cvs on Sat Aug 11 19:43:20 2007 from C:\Documents and Settings\Joaquin\Escritorio\ITRAQ4.wxg
#
"""
xtraq_class_main_10.py
(xtraq_36)
"""
#
import wx
import base64
import cStringIO
from xtraq_images import LAMA, ICON
#
#
FRAME_TITLE = "  THE ITRAQ HACKER MAIN CLASS 1.0"
#
#
class MyFrame(wx.Frame):
    def __init__(self, *args, **kwds):
        # begin wxGlade: MyFrame.__init__
        kwds["style"] = wx.DEFAULT_FRAME_STYLE|wx.FULL_REPAINT_ON_RESIZE
        wx.Frame.__init__(self, *args, **kwds)
        #
        self.lama = None
        self.property = {}
        #
        self.get_lama(LAMA)
        self.set_icon(ICON)
        #
        self.statusbar = self.CreateStatusBar(1, 0)
        self.tc_main = wx.TextCtrl(self, -1, "", style=wx.TE_MULTILINE|wx.TE_READONLY)
        self.lb_flin = wx.StaticText(self, -1, "SOURCE   ")
        self.tc_flin = wx.TextCtrl(self, -1, "")
        self.bt_flin = wx.Button(self, -1, "...")
        self.lb_flout = wx.StaticText(self, -1, "FILTERED ")
        self.tc_flout = wx.TextCtrl(self, -1, "")
        self.bt_flout = wx.Button(self, -1, "...")
        self.lb_fxls = wx.StaticText(self, -1, "REPORT   ")
        self.tc_fxls = wx.TextCtrl(self, -1, "")
        self.bt_fxls = wx.Button(self, -1, "...")
        self.bt_siit = wx.Button(self, -1, "")
        self.rbx_op = wx.RadioBox(self, -1, "action", choices=["FILTER", "REPORT"], majorDimension=1, style=wx.RA_SPECIFY_ROWS)
        self.rbx_tp = wx.RadioBox(self, -1, "tipo archivo", choices=[".dta        ", ".anz"], majorDimension=1, style=wx.RA_SPECIFY_ROWS)
        self.bt_run = wx.Button(self, -1, "RUN")
        self.bmp = wx.StaticBitmap(self, -1, wx.BitmapFromImage(self.lama))
        self.cbx_bcht = wx.CheckBox(self, -1, "bacht")
        self.tc_sec = wx.TextCtrl(self, -1, "", style=wx.TE_MULTILINE|wx.TE_READONLY)
        self.lb_ftd = wx.StaticText(self, -1, "     filtrado")
        self.tc_cffitraq = wx.TextCtrl(self, -1, "")
        self.lb_cffitraq = wx.StaticText(self, -1, "cutoff itraq")
        self.lb_rprt = wx.StaticText(self, -1, "   report")
        self.tc_cffscr = wx.TextCtrl(self, -1, "")
        self.lb_cffscr = wx.StaticText(self, -1, "cutoff score")
        self.tc_cfftgs = wx.TextCtrl(self, -1, "")
        self.lb_cfftgs = wx.StaticText(self, -1, "cutoff tags")
        self.lb_quant = wx.StaticText(self, -1, "     itraq")
        self.bt_iso = wx.Button(self, -1, "isotopes")
        self.bt_cal = wx.Button(self, -1, "calibration")
        self.lb_spec = wx.StaticText(self, -1, "  spectra")
        self.bt_spec = wx.Button(self, -1, "ions")
        self.lb_report = wx.StaticText(self, -1, "Report Data", style=wx.ALIGN_CENTRE)
        #
        self.cbx_list = ['scx', 'bseq', 'scn', 'bscr', 'mp', 'pktgs', 'chg', 'fpks',
                         'ref', 'itrq', 'quant', 'xprt']
                        
        self.cbx_dict = dict(scx="LC_Run", bseq="best sequence",
                             scn="scan", bscr="scores",
                             mp="parent mass", pktgs="peaks tags",
                             chg="charge", fpks="base peaks",
                             ref="reference", itrq="iTRAQ ions",
                             quant="Quant iTRAQ", xprt="Export Tags")
        
        for cbx, label in self.cbx_dict.iteritems():
            name = 'cbx_' + cbx
            setattr(self, name, wx.CheckBox(self, -1, label))
        #
        self.lb_idx = wx.StaticText(self, -1, "Indices", style=wx.ALIGN_CENTRE)
        self.cbx_idx = wx.CheckBox(self, -1, "use it  ")
        self.tc_fidx = wx.TextCtrl(self, -1, "")
        self.bt_fidx = wx.Button(self, -1, "...")
                    
        self.__set_properties()
        self.__do_layout()
        self.getframeparams()
        
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: MyFrame.__set_properties
        self.SetTitle(FRAME_TITLE)
        self.SetMinSize((500, 420))
        self.SetBackgroundColour(wx.Colour(253, 255, 164))
        self.statusbar.SetStatusWidths([-1])
        # statusbar fields
        statusbar_fields = ["The iTRAQ Hacker -- Bienvenido!"]
        for i in range(len(statusbar_fields)):
            self.statusbar.SetStatusText(statusbar_fields[i], i)
        self.tc_main.SetMinSize((500, 130))
        self.bt_flin.SetMinSize((45, 20))
        self.bt_flout.SetMinSize((45, 20))
        self.bt_fxls.SetMinSize((25, 20))
        self.bt_siit.SetMinSize((15, 15))
        self.rbx_op.SetSelection(0)
        self.rbx_tp.SetSelection(1)
        self.tc_sec.SetMinSize((500, 50))
        self.tc_cffitraq.SetMinSize((50, 20))
        self.tc_cffscr.SetMinSize((50, 20))
        self.tc_cfftgs.SetMinSize((50, 20))
        self.bt_iso.SetMinSize((50, 20))
        self.bt_cal.SetMinSize((50, 20))
        self.bt_spec.SetMinSize((50, 20))
        self.lb_report.SetMinSize((218, 20))
        
        for cbx in self.cbx_dict:
            name = 'cbx_' + cbx
            f = getattr(self, name)
            f.SetValue(1)
            
        self.lb_idx.SetBackgroundColour(wx.Colour(250, 255, 15))
        self.bt_fidx.SetMinSize((40, 20))

    def __do_layout(self):
        # begin wxGlade: MyFrame.__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_10 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_18 = wx.BoxSizer(wx.VERTICAL)
        sizer_19 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_9 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_12 = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_1 = wx.GridSizer(6, 2, 3, 0)
        sizer_4 = wx.BoxSizer(wx.VERTICAL)
        sizer_11 = wx.BoxSizer(wx.VERTICAL)
        sizer_15 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_14 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_16 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_13 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_17 = wx.BoxSizer(wx.VERTICAL)
        sizer_8 = wx.BoxSizer(wx.VERTICAL)
        sizer_3 = wx.BoxSizer(wx.VERTICAL)
        sizer_5 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_6 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_7 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_1.Add(self.tc_main, 1, wx.ALL|wx.EXPAND, 7)
        label_1 = wx.StaticText(self, -1, "Archivos", style=wx.ALIGN_CENTRE)
        label_1.SetBackgroundColour(wx.Colour(255, 255, 0))
        sizer_1.Add(label_1, 0, wx.TOP|wx.BOTTOM|wx.EXPAND, 2)
        sizer_7.Add(self.lb_flin, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_7.Add(self.tc_flin, 1, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_7.Add(self.bt_flin, 0, wx.RIGHT|wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_3.Add(sizer_7, 1, wx.EXPAND, 0)
        sizer_6.Add(self.lb_flout, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_6.Add(self.tc_flout, 1, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_6.Add(self.bt_flout, 0, wx.RIGHT|wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_3.Add(sizer_6, 1, wx.EXPAND, 0)
        sizer_5.Add(self.lb_fxls, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_5.Add(self.tc_fxls, 1, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_5.Add(self.bt_fxls, 0, wx.RIGHT|wx.ALIGN_CENTER_VERTICAL, 2)
        sizer_5.Add(self.bt_siit, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_3.Add(sizer_5, 1, wx.EXPAND, 0)
        sizer_2.Add((10, 20), 0, wx.EXPAND, 0)
        sizer_2.Add(sizer_3, 1, wx.EXPAND, 0)
        sizer_2.Add((20, 20), 0, wx.EXPAND, 0)
        sizer_8.Add(self.rbx_op, 0, wx.EXPAND, 0)
        sizer_8.Add(self.rbx_tp, 0, wx.EXPAND, 0)
        sizer_8.Add(self.bt_run, 0, wx.ALL|wx.EXPAND, 6)
        sizer_2.Add(sizer_8, 0, wx.EXPAND, 0)
        sizer_2.Add((20, 20), 0, wx.EXPAND, 0)
        sizer_17.Add(self.bmp, 0, wx.ALL|wx.EXPAND, 3)
        sizer_17.Add(self.cbx_bcht, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 8)
        sizer_2.Add(sizer_17, 0, wx.EXPAND, 0)
        sizer_2.Add((10, 20), 0, wx.EXPAND, 0)
        sizer_1.Add(sizer_2, 0, wx.EXPAND, 0)
        sizer_1.Add(self.tc_sec, 0, wx.EXPAND, 0)
        label_2 = wx.StaticText(self, -1, "Parameters", style=wx.ALIGN_CENTRE)
        label_2.SetBackgroundColour(wx.Colour(250, 255, 15))
        sizer_1.Add(label_2, 0, wx.TOP|wx.BOTTOM|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 2)
        sizer_9.Add((20, 20), 0, wx.EXPAND, 0)
        sizer_11.Add(self.lb_ftd, 0, 0, 0)
        sizer_13.Add(self.tc_cffitraq, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 1)
        sizer_13.Add(self.lb_cffitraq, 0, wx.LEFT|wx.ALIGN_CENTER_VERTICAL, 4)
        sizer_11.Add(sizer_13, 1, wx.EXPAND, 0)
        sizer_16.Add(self.lb_rprt, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        sizer_11.Add(sizer_16, 1, wx.EXPAND, 0)
        sizer_14.Add(self.tc_cffscr, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 1)
        sizer_14.Add(self.lb_cffscr, 0, wx.LEFT|wx.ALIGN_CENTER_VERTICAL, 4)
        sizer_11.Add(sizer_14, 1, wx.EXPAND, 0)
        sizer_15.Add(self.tc_cfftgs, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 1)
        sizer_15.Add(self.lb_cfftgs, 0, wx.LEFT|wx.ALIGN_CENTER_VERTICAL, 4)
        sizer_11.Add(sizer_15, 1, wx.EXPAND, 0)
        sizer_9.Add(sizer_11, 0, 0, 0)
        sizer_9.Add((10, 20), 1, wx.EXPAND, 0)
        sizer_4.Add(self.lb_quant, 0, wx.ALL, 2)
        sizer_4.Add(self.bt_iso, 0, wx.ALL, 2)
        sizer_4.Add(self.bt_cal, 0, wx.ALL, 2)
        sizer_4.Add(self.lb_spec, 0, wx.TOP, 5)
        sizer_4.Add(self.bt_spec, 0, wx.ALL, 2)
        sizer_9.Add(sizer_4, 0, 0, 0)
        sizer_9.Add((20, 20), 1, wx.EXPAND, 0)
        sizer_12.Add(self.lb_report, 0, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 1)
        ##
        for cbx in self.cbx_list:
            name = 'cbx_' + cbx
            grid_sizer_1.Add(getattr(self, name), 0, wx.LEFT, 20)
        ##
        sizer_12.Add(grid_sizer_1, 0, wx.EXPAND, 0)
        sizer_9.Add(sizer_12, 0, wx.EXPAND, 0)
        sizer_9.Add((20, 20), 0, wx.EXPAND, 0)
        sizer_1.Add(sizer_9, 0, wx.EXPAND, 0)
        sizer_18.Add(self.lb_idx, 0, wx.TOP|wx.BOTTOM|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 2)
        sizer_19.Add(self.cbx_idx, 0, wx.ALL, 8)
        sizer_19.Add(self.tc_fidx, 1, wx.TOP|wx.BOTTOM, 2)
        sizer_19.Add(self.bt_fidx, 0, wx.TOP|wx.BOTTOM, 2)
        sizer_18.Add(sizer_19, 0, wx.EXPAND, 0)
        sizer_10.Add(sizer_18, 1, wx.EXPAND, 0)
        sizer_10.Add((240, 40), 0, wx.EXPAND, 0)
        sizer_1.Add(sizer_10, 0, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        sizer_1.Fit(self)
        self.Layout()
        # end wxGlade
        
    def decode_string(self, string):
        data = base64.decodestring(string)
        flhndl = cStringIO.StringIO(data)
        return wx.ImageFromStream(flhndl, wx.BITMAP_TYPE_ANY)
        
    def get_lama(self, img):
        """"""
        self.lama = self.decode_string(img)
        
    def set_icon(self, ico):
        """"""
        img = self.decode_string(ico)
        _icon = wx.IconFromBitmap(wx.BitmapFromImage(img))
        self.SetIcon(_icon)
        
    def getframeparams(self):
        """
        tc_flin, tc_flout, tc_fxls, tc_cffitraq, tc_cffscr, tc_cfftgs  
        
        rbx_op, rbx_tp  
        
        cbx_bcht, cbx_scx, cbx_bseq, cbx_scn, cbx_bscr, cbx_mp  
        cbx_pktgs, cbx_chg, cbx_fpks, cbx_ref, cbx_itrq 
        """ 
        checkboxes = ['cbx_', 'IsChecked', 'bcht', 'idx'] + self.cbx_list
        textctrls = ['tc_', 'GetValue', 'flin', 'flout', 'fxls', 'cffitraq', 'cffscr', 'cfftgs', 'fidx']
        radioboxes = ['rbx_', 'GetSelection', 'op', 'tp']
        for group in [checkboxes, textctrls, radioboxes]:
            prefix = group[0]
            function = group[1]
            for objeto in group[2:]:
                name = prefix + objeto
                self.property[objeto] = getattr(getattr(self, name), function)()
                #print name, self.property[objeto]
        
        
class MyApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        frame_1 = MyFrame(None, -1, "")
        self.SetTopWindow(frame_1)
        frame_1.Show()
        return 1

# end of class MyApp

if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()