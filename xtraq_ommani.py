#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
xtraq_ommani_01.py
(xtraq_36)
"""
#
import os
import wx
import wx.animate
import base64
from xtraq_images import OMMANI
#
#
class OmmaniPanel(wx.Panel):
    def __init__(self, parent, size=(100, 100), position=(10, 10)):
        wx.Panel.__init__(self, parent, -1, size=size, pos=position)
        img = base64.decodestring(OMMANI)
        open('temp', 'wb').write(img)
        while True:
            try:
                ani = wx.animate.Animation('temp')
                break
            except IOError:
                pass   
       
        ctrl = wx.animate.AnimationCtrl(self, -1, ani)
        ctrl.Play()
        
class ManiClock(wx.Frame):
    def __init__(self, *args, **kargs):
        kargs["style"] = wx.CAPTION|wx.CLOSE_BOX|wx.SYSTEM_MENU|wx.FRAME_TOOL_WINDOW|wx.STAY_ON_TOP
        kargs["size"] = (105, 120)
        wx.Frame.__init__(self, *args, **kargs)
        self.SetMaxSize((105, 120))
        self.pan = OmmaniPanel(self)
        self.Bind(wx.EVT_CLOSE, self.on_close_window)
    
    def on_close_window(self, evt):
        os.remove('temp')
        self.Destroy()

if __name__ == "__main__":
    
    app = wx.PySimpleApp()
    frame = ManiClock(None)
    frame.Show()
    app.MainLoop()